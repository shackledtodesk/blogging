#!/bin/bash

## html-reform.sh
##
## Take the HTML from blogger convert to markdown,
## download the images, and reform the links.
##
## Currently takes no arguments.  
## Requires: ruby gem html2markdown (gem install html2markdown)
## You need to minimally set the workDir, the location of
## where the Jekyll importer created the _posts and _drafts
## directories.

## post directory setups
workDir="/Users/andy/Documents/personal/tmp"
postDir="_posts"
mdDir="_md"
imgDir="images/posts"

pop=`pwd`
cd ${workDir}

## create the directories for writing if they don't exist
mkdir -p ${mdDir}
rm -f ${mdDir}/*
mkdir -p ${imgDir}
rm -f ${imgDir}/*

## Download an image from url, place into imgDir making sure
## it is unique.  
## Imaged saved as: images/posts/postName-[imgNameNoExtention][possibleCounter].ext
function dlImg() {
  mdName=$1
  postName=$2
  url=$3
  
  ## get the base filename and strip urlencoding
  imgFile=`basename ${url} | awk '{printf RT?$0chr("0x"substr(RT,2)):$0}' RS=%..`

  if [ -z "${imgFile}" ]; then
      return
  fi

  ## Download file to tmp location
  curl -s "${url}" > "/tmp/${postName}-${imgFile}"
  echo -n "."

  ## Check for pre-existing image file
  if [ -f "${imgDir}/${postName}-${imgFile}" ]; then
      let count=0
      ext=${imgFile: -4}
      fName=${imgFile%.*}

      ## iterate until filename is unique
      while [ -f "${imgDir}/${postName}-${fName}-${count}${ext}" ]; do
	  let count=count+1
      done

      ## Copy tmp file to perm location and name
      mv "/tmp/${postName}-${imgFile}" "${imgDir}/${postName}-${fName}-${count}${ext}"
      imgFile="${fName}-${count}${ext}"
      echo -n "."
  else
      mv "/tmp/${postName}-${imgFile}" "${imgDir}/${postName}-${imgFile}"
  fi

  ## Replace link in document
  mdFile="${mdDir}/${postName}.md"
  sed -i .bak -e "s#${url}#/${imgDir}/${postName}-${imgFile}#g" ${mdFile}
  rm ${mdFile}.bak
}

## Find image links in file
function getImg() {
    mdName=$1
    postName=$2

    IFS=$'('; for hunt in `cat $mdName`; do
	url=`echo ${hunt} | cut -f1 -d\)`
	url=`IFS=$' \t\n'; echo ${url} | awk '{ print $1 }'`
	if [[ "${url}" == "http"* ]]; then
	    pref=${url:0:5}
	    ext=${url: -3}
	    if [ "${ext}" = "png" ] || 
		[ "${ext}" = "gif" ] || 
		[ "${ext}" = "jpg" ]; then
		dlImg "${mdName}" "${postName}" "${url}"
	    fi
	fi
    done

    ## replace thumbnail image in header
    mdFile="${mdDir}/${postName}.md"
    url=`grep -E "^thumbnail:" "${mdFile}" | awk '{ print $2 }'`
    if [ ! -z "${url}" ]; then
	dlImg "${mdName}" "${postName}" "${url}"
    fi

}

## Attempt to fix extra line spacing in code
function fixCode() {
    mdFile=$1
    inCode=0
    oneSpace=0
    let lncount=0

    hasCode=`grep -c "~~~" ${mdFile}`
    if [ "${hasCode}" -eq 0 ]; then
	return
    fi

    ## IFS=$'\n'; for i in `cat `; do
    while read i; do
	## echo "LINE: $i"
	if [ ${inCode} -eq 1 ]; then
	    if [ ! -z "${i}" ] || [ ${oneSpace} -eq 0 ]; then
		let lncount=$lncount+1
		if [[ "${i}" = "~~~"* ]]; then
		    inCode=0
		    i=`echo ${i} | sed -E 's,^(\~\~\~)(.*),\1\'$'\n''\'$'\n''\2,'`
		    echo -e "${i}" >> /tmp/thing-$$.md
		    ## echo "${lncount} end"
		else
		    echo -e "${i}" >> /tmp/thing-$$.md
		fi
		## echo "CODE: $i"

		oneSpace=0
	    elif [ ${oneSpace} -eq 0 ]; then
		oneSpace=1
		## echo "NOT SAVED"
	    fi
	else
	    ## echo "NORM: $i"
	    if [ "${i}" = '~~~' ]; then
		inCode=1
		oneSpace=0
		## echo "start? ${i}"
		
		echo -e "\n${i}" >> /tmp/thing-$$.md
	    else
		## echo  >> /tmp/thing-$$.md
		echo -e "${i}" >> /tmp/thing-$$.md
	    fi
	fi
    done <"${mdFile}"

    mv /tmp/thing-$$.md ${mdFile}
    echo -n "."
}

## Loop through HTML files in post directory...
for i in `ls ${postDir}/*.html`; do  
    ## create a postname
    fname=`basename $i | sed -e 's/\.html$//'`
    echo -n "Processing: ${fname} ."
    ## convert to markdown
    ## A bunch of real ugly sed regex to try and clean up the HTML and markdown
    ## since what I got back from Blogger is a tangled mess
    ## First 4 lines are for code blocks
    ## 5th line pre-marks image captions
    ## 6th line coverts html to markdown
    ## 7th fix spacing for headers
    ## 8th fix spacing for unordered lists
    ## 9th line replaces pre-marks for captions with requisite HTML
    ## 10th remove extraneous pre-markers
    ## 11th ensure spacing after document header and pipe results to file
    cat $i \
	| sed -e 's,\<blockquote\>\<pre\>,\'$'\n''~~~\'$'\n'',g' \
	| sed -e 's,\</pre\>\</blockquote\>,\'$'\n''~~~\'$'\n'',g' \
	| sed -E '/div class\=\"codeContent/ s,(\<div class\=\"codeContent),\'$'\n''~~~\'$'\n''\1,' \
	| sed -e '/div class\=\"codeContent/!b' -e ':a' -e 's,\</div\>,\</div\>\'$'\n''~~~\'$'\n'',;t' -e 'n;ba' -e ':trail' -e 'n;btrail' \
	| sed -e '/class\=\"tr-caption/!b' -e ':a' -e 's,\</table\>,\</table\>@=@,g;t' -e 'n;ba' -e ':trail' -e 'n;btrail' \
	| ruby -e 'require "html2markdown"; puts HTMLPage.new(contents: STDIN.read).markdown' \
	| sed -E '/^#+[[:alpha:]]/ s/(^#+)(.*)/\'$'\n''\1 \2\'$'\n''/g' \
	| sed -E 's/(^\*+)(.*)/\'$'\n''\1 \2/g' \
	| sed -E 's,(\[!\[[^]]*\]\(.*\)\]\([^\)]+\))(.*),\1\'$'\n''\<br\>\2\'$'\n'',g' \
	| sed -e 's,\(.*\)@=@,\<sup\>\<sub\>\1\'$'\n''\'$'\n'',g' \
	| sed -e 's,@=@,,g' \
	| sed -E 's/(\-{3})(.)/\1\'$'\n''\2/g' > "${mdDir}/${fname}.md"

    ## get images and update links
    getImg "${mdDir}/${fname}.md" "${fname}"
    ## attempt to remove extraneous spaces in code blocks
    fixCode "${mdDir}/${fname}.md"
    echo
done

cd ${pop}